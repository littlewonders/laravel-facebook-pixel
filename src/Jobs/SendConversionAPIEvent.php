<?php

namespace Littlewonders\FacebookPixel\Jobs;

use Littlewonders\FacebookPixel\Facades\FacebookPixel;
use FacebookAds\Object\ServerSide\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendConversionAPIEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function handle()
    {
        FacebookPixel::sendEventSync($this->event);
    }
}