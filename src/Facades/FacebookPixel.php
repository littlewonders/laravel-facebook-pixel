<?php

namespace Littlewonders\FacebookPixel\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Littlewonders\FacebookPixel\FacebookPixel
 */
class FacebookPixel extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \Littlewonders\FacebookPixel\FacebookPixel::class;
    }
}
