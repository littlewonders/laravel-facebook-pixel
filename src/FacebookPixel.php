<?php

namespace Littlewonders\FacebookPixel;

use Littlewonders\FacebookPixel\Jobs\SendConversionAPIEvent;
use Exception;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\ServerSide\ActionSource;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Traits\Macroable;

class FacebookPixel
{
    use Macroable;

    protected bool $enabled;
    protected string $pixelId;
    protected string $token;
    protected string $sessionKey;
    protected array $cookies;
    protected bool $logEvents;
    protected bool $advancedMatching;
    protected ?string $testCode;
    protected EventLayer $eventLayer;
    protected EventLayer $customEventLayer;
    protected EventLayer $flashEventLayer;

    public function __construct()
    {
        $this->enabled = config('facebook-pixel.enabled');
        $this->pixelId = config('facebook-pixel.facebook_pixel_id');
        $this->token = config('facebook-pixel.token');
        $this->sessionKey = config('facebook-pixel.sessionKey');
        $this->cookies = config('facebook-pixel.cookies');
        $this->logEvents = config('facebook-pixel.logEvents');
        $this->testCode = config('facebook-pixel.eventTestCode');
        $this->advancedMatching = config('facebook-pixel.advancedMatching');
        $this->eventLayer = new EventLayer();
        $this->customEventLayer = new EventLayer();
        $this->flashEventLayer = new EventLayer();
    }

    public function pixelId()
    {
        return $this->pixelId;
    }

    public function sessionKey()
    {
        return $this->sessionKey;
    }

    public function token()
    {
        return $this->token;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function enable()
    {
        $this->enabled = true;
    }

    public function disable()
    {
        $this->enabled = false;
    }

    /**
     * Add event to the event layer.
     *
     */
    public function track(string $eventName, array $parameters = [])
    {
        $this->eventLayer->set($eventName, $parameters);
    }

    /**
     * Add custom event to the event layer.
     *
     */
    public function trackCustom(string $eventName, array $parameters = [])
    {
        $this->customEventLayer->set($eventName, $parameters);
    }

    /**
     * Add event data to the event layer for the next request.
     *
     */
    public function flashEvent(string $eventName, array $parameters = [])
    {
        $this->flashEventLayer->set($eventName, $parameters);
    }

    /**
     * Queue a server-side event to also be tracked via the client pixel
     * 
     */
    public function sendServerEventViaClient(Event $event)
    {
        $parameters = $event->getCustomData()->normalize() ?? [];
        $parameters['matching'] = ["eventID" => $event->getEventId()];

        $this->track($event->getEventName(), $parameters);
    }

    /**
     * If enabled in the configuration, add the _fbp and _fbc cookies to the event
     * 
     */
    public function applyCookiesToUserData(UserData $userData) {
        if (in_array('_fbp', $this->cookies) && isset($_COOKIE['_fbp'])) {
            $userData->setFbp((string)$_COOKIE['_fbp']);
        }
        if (in_array('_fbc', $this->cookies) && isset($_COOKIE['_fbc'])) {
            $userData->setFbc((string)$_COOKIE['_fbc']);
        }
    }

    /**
     * Generate a UserData object with the Client IP, User Agent, and configured cookies
     * 
     */
    public function generateUserData() : UserData {
        $userData = (new UserData())
            ->setClientIpAddress(request()->ip())
            ->setClientUserAgent(request()->userAgent());
        $this->applyCookiesToUserData($userData);
        return $userData;
    }

    /**
     * Send an event through both the client side API and the conversions API
     * 
     */
    public function sendAndTrack(string $eventName, CustomData $customData, UserData $userData = null)
    {
        $event = $this->makeEvent($eventName, request()->url(), $customData, $userData);
        $this->sendEvent($event, true);
        $this->sendServerEventViaClient($event);
    }

    /**
     * Generate a random ID for matching client and server events
     * 
     */
    private function generateEventId() {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < 24; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

    private function makeEvent(string $eventName, string $sourceUrl, CustomData $customData, UserData $userData = null) {
        return (new Event())
            ->setEventName($eventName)
            ->setEventTime(time())
            ->setEventSourceUrl($sourceUrl)
            ->setEventId($this->generateEventId())
            ->setUserData($userData ?? $this->generateUserData())
            ->setCustomData($customData)
            ->setActionSource(ActionSource::WEBSITE);
    }

    /**
     * Make and send event using Conversions API
     *
     */
    public function send(string $eventName, string $sourceUrl, CustomData $customData, UserData $userData = null, $queue = true)
    {
        $event = $this->makeEvent($eventName, $sourceUrl, $customData, $userData);
        $this->sendEvent($event, $queue);
    }

    /**
     * Send event using Conversions API
     * 
     */
    public function sendEvent(Event $event, $queue)
    {
        if (! $this->isEnabled()) {
            return null;
        }
        if (empty($this->token())) {
            throw new Exception('You need to set a token in your .env file to use the Conversions API.');
        }

        $api = Api::init(null, null, $this->token);
        $api->setLogger(new CurlLogger());

        return 
            $queue ?
            $this->sendEventSync($event) :
            $this->sendEventAsJob($event);

    }

    /**
     * Dispatch a Conversion API call synchronously
     * 
     */
    public function sendEventSync(Event $event) {
        $events = [];
        array_push($events, $event);

        $request = (new EventRequest($this->pixelId()))->setEvents($events);
        if ($this->testCode) $request->setTestEventCode($this->testCode);

        if ($this->logEvents)
            Log::info("Sent '{$event->getEventName()}' to Facebook", $event->normalize());

        try {
            return $request->execute();
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    /**
     * Dispatch a Conversion API call as a job
     * 
     */
    public function sendEventAsJob(Event $event) {
        if ($this->logEvents)
            Log::info("Queueing '{$event->getEventName()}' to be sent");

        SendConversionAPIEvent::dispatch($event);
    }

    /**
     * Merge array data with the event layer.
     *
     */
    public function merge(array $eventSession)
    {
        $this->eventLayer->merge($eventSession);
    }

    /**
     * Retrieve the event layer.
     *
     */
    public function getEventLayer(): EventLayer
    {
        return $this->eventLayer;
    }

    /**
     * Retrieve the event layer and also delete it at the same time
     *
     */
    public function getAndClearEventLayer(): array
    {
        $layer = $this->eventLayer->toArray();
        $this->eventLayer->clear();
        return $layer;
    }

    

    /**
     * Retrieve custom event layer.
     *
     */
    public function getCustomEventLayer(): EventLayer
    {
        return $this->customEventLayer;
    }

    /**
     * Retrieve the event layer's data for the next request.
     *
     */
    public function getFlashedEvent()
    {
        return $this->flashEventLayer->toArray();
    }

    /**
     * Retrieve the email to use it advanced matching.
     * To use advanced matching we will get the email if the user is authenticated
     */
    public function getEmail()
    {
        if (!$this->advancedMatching) return null;
        
        if (Auth::check()) {
            return Auth::user()->email;
        }

        return null;
    }

    public function clear()
    {
        $this->eventLayer = new EventLayer();
    }
}
