<?php

namespace Littlewonders\FacebookPixel;

use Closure;
use Illuminate\Session\Store as Session;
use Illuminate\Support\Facades\Log;

class FacebookPixelMiddleware
{
    protected $facebookPixel;
    protected $session;

    public function __construct(FacebookPixel $facebookPixel, Session $session)
    {
        $this->facebookPixel = $facebookPixel;
        $this->session = $session;
    }

    public function handle($request, Closure $next)
    {
        if ($this->session->has($this->facebookPixel->sessionKey())) {
            $this->facebookPixel->merge($this->session->get($this->facebookPixel->sessionKey()));
        }
        $response = $next($request);

        $layer = $this->facebookPixel->getEventLayer()->toArray();
        if (count($layer))
            Log::debug("Facebook Pixel - Saving events " . print_r(array_keys($layer), true) . " until next loop");
            
        $this->session->flash($this->facebookPixel->sessionKey(), $layer);

        return $response;
    }
}
