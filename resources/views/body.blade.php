@if($enabled)
@unless(empty($eventLayer))
<!-- Facebook Pixel Events -->
<script>
@foreach($eventLayer as $eventName => $parameters)
    <?php
        $matching = $parameters['matching'] ?? [];
        unset($parameters['matching']);
    ?>
    var parameters = {{ Illuminate\Support\Js::from($parameters) }};
    var matching = {{ Illuminate\Support\Js::from($matching) }};

    fbq('track', '{{ $eventName }}', parameters, matching);
@endforeach
</script>
<!-- End Facebook Pixel Events -->
@endunless
@unless(empty($customEventLayer->toArray()))
<!-- Facebook Pixel Custom Events -->
<script>
@foreach($customEventLayer->toArray() as $customEventName => $parameters)
    <?php
        $matching = $parameters['matching'] ?? [];
        unset($parameters['matching']);
    ?>
    var parameters = {{ Illuminate\Support\Js::from($parameters) }};
    var matching = {{ Illuminate\Support\Js::from($matching) }};

    fbq('trackCustom', '{{ $customEventName }}', parameters, matching);
@endforeach
</script>
<!-- End Facebook Custom Pixel Events -->
@endunless
@endif
